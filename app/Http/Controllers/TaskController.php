<?php

namespace App\Http\Controllers;

use App\Task;
use Auth;
use DB;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request, [
            'task' => 'required|string',
            'date' => 'string',
            'importance' => 'nullable',

        ]);


        $id = Auth::user()->id;

        $task = new Task();

        $task->user_id = $id;

            $task->task = $request['task'];
            $task->date = $request['date'];
            $task->importance = $request['importance'];
            $task->save();

        return redirect ('home')->with('success', 'Task created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        $tasks = DB::table('tasks')
        ->where('user_id','=',Auth::user()->id)
        ->where('deleted_at','=', null)
        ->get();

        // return $tasks;

        return view('home',compact('tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task, $id)
    {
        $tasks = DB::table('tasks')
        ->where('id','=',$id)
        ->get();

        return view('update', compact('tasks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $this->validate($request, [
            'task' => 'required|string',
            'date' => 'string',
            'importance' => 'nullable',

        ]);

        $id = $request['id'];


        $tasks= Task::where('id', '=', $id)->first();
        $updated= $tasks->update([

        'task' => $request['task'],
        'date' => $request['date'],
        'importance' => $request['importance'],
        ]);


        return redirect('home')->with('success','Task updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task, $id)
    {

        $tasks = Task::destroy($id);

        return redirect()->back()
        ->with('success','Task deleted sucessfully');
    }

    public function statusUpdate($id){

        // return $id;
        $identification = $id;
        $tasks= Task::where('id', '=', $id)->first();

        $updated = $tasks->update([
            'status'=>'complete'
        ]);

        return redirect('home')->with('success','Task completed');


    }

    public function completed(){

        $tasks = DB::table('tasks')
        ->where('user_id','=',Auth::user()->id)
        ->where('deleted_at','=', null)
        ->where('status','=','complete')
        ->get();

        return view('completed',compact('tasks'));


    }

}
