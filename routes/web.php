<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::post('newtask', 'TaskController@store')->name('task');

Route::get('home','TaskController@show')->name('home');

Route::get('edittask/{id}', 'TaskController@edit')->name('editTask');

Route::post('updatetask/{id}', 'TaskController@update')->name('updateTask');

Route::get('deletetask/{id}', 'TaskController@destroy')->name('deleteTask');

Route::get('updatestatus/{id}', 'TaskController@statusUpdate')->name('updateStatus');

Route::get('completedtasks','TaskController@completed')->name('completedTasks');



