<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/landingpage.css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Welcome</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <section>
            <nav class="navbar navbar-expand-md navbar-light bg-light sticky-top" id="/">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"><img src="../images/logo.png" id="logo"/></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">

                                <li class="nav-item">
                                    <a class="nav-link li-modal" href="{{route('login')}}"><p id="link">{{ __('Login') }}</p></a>

                                </li>

                                <li class="nav-item">

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </section>

        <section>
            <div id="welcometext">
                <h1 id="lineone"><strong> What are your plans for today? <strong></h1>
                <small id="linetwo"> Welcome, to Done?, your favourite online todo list planner</small>
            </div>


            <div id="button">
                <div id="landingbutton">
                    <button type="button" class="btn btn-default" id="yellowbutton">
                        <a class="nav-link li-modal" id="buttontext" href="{{route('register')}}"><p id="link">Register</p></a>
                    </button>

                </div>
            </div>

            </section>
        </div>
    </body>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>
