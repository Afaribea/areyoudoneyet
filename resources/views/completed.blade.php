<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/landingpage.css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>

    <body>
        <section>
            <nav class="navbar navbar-expand-md navbar-light bg-light sticky-top" >
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"><img src="../images/logo.png" id="logo"/></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a href="{{ url('home') }}" class="nav-link"> Home </a>
                                </li>

                                <li class="nav-item dropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </section>

        @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
            <button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        @endif


        {{-- // main display --}}
        <section id="todostart">
            <div class="row" id="todo">
                <div class="col-md-12" >

                    <div class="card" id="todolist">
                        <div class="card-body">
                            <hr>
                            <h5 class="card-subtitle mb-2 text-muted">what do you have to do?</h5>
                            <br>
                            <div>
                                <table class="table" id="tabledesign">
                                    <thead>
                                      <tr>
                                        <th class="cell100 column2">Task</th>
                                            <th class="cell100 column3">Complete by</th>
                                            <th class="cell100 column4">Importance</th>
                                            <th class="cell100 column5">Status</th>
                                            <th class="cell100 column6">Delete</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tasks as $task)
                                                <tr class="row100 body">
                                                        <td class="cell100 column2">{{ $task->task }}</td>
                                                        <td class="cell100 column3">{{ $task->date }}</td>
                                                        <td class="cell100 column4">{{ $task->importance }}</td>
                                                        <td class="cell100 column5">{{ $task->status }}</td>
                                                        <td class="cell100 column6"><a href="{{ route('deleteTask', $task->id) }}">
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="click to delete entry">Delete</button>
                                                        </a>
                                                        </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                            </div>
                        </div>
                      </div>
                </div>
            </div>
        </section>


        <section>
            {{-- edit task --}}
            @foreach ($tasks as $task )
            <div class="modal" tabindex="-1" id="edit">
                <div class="modal-dialog modal-dialog-centered modal-lg" id="popupbox">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Update Task</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                    <div class="modal-body">
                        <form action ="#" method="POST">

                            @csrf

                            {{-- <input type="hidden" name="id" value="{{ $task->id }}"> --}}
                            <div class="form-group">
                                <label for="task" id="task"><h5 id="title">Task</h5></label>
                                <input type="text" class="form-control" id="task" name="task" placeholder="Input the task" >
                            </div>
                            @error('task')
                                <p class="help is-danger">{{ $message }}</p>
                            @enderror

                            <div class="form-group">
                                <label for="date" id="date"><h5 id="title">Complete by</h5></label>
                                <input type="date" name="date" class="form-control" id="date" >
                            </div>

                            <div class="form-group">
                                <label for="Status" id="importance"><h5 id="title">Importance</h5></label>
                                <select class="form-control" id="importance" name="importance">
                                <option value="urgent">Urgent</option>
                                <option value="not too urgent">Not too urgent</option>
                                </select>
                            </div>


                            <div class="modal-footer">
                                <input type="submit" id="submitBtn"  class="btn btn-primary" name="submit" value="Submit">
                            </div>

                        </form>

                    </div>
                  </div>
                </div>
              </div>
              @endforeach
        </section>


        @include('sweetalert::alert')
    </body>

    <script>
        $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</html>










